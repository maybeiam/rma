//
//  ProductViewModel.swift
//  RMA
//
//  Created by 1002220 on 2016. 10. 10..
//  Copyright © 2016년 Jin. All rights reserved.
//

import UIKit
import ReactiveSwift
import Result

class ProductViewModel: BaseViewModel {
    fileprivate var productId : String
    fileprivate let rac_product = MutableProperty<PRODUCT?>(nil)
    let rac_price = MutableProperty<String>("")
    let rac_title = MutableProperty<String>("")
    let rac_desc = MutableProperty<String>("")
    let rac_cells = MutableProperty<Array<ProductCellViewModel>>(Array<ProductCellViewModel>())
    let rac_itemCount = MutableProperty<Int>(0)
    let rac_loaded = MutableProperty<Bool>(false)
    
    var imageCount : Int {
        return self.rac_itemCount.value
    }
    
    override init() {
        self.productId = ""
    }
    
    init( withProductId productId : String ) {
        self.productId = productId
    }
    
    func bind() {
        PRODUCT_PRODUCTDETAIL.downloadProductDetailFromServer(productId: self.productId)
        .mapError { _ in
            return FetchError.APIError
        }
        .startWithSignal { [unowned self] (signal, disposable) in
            signal.observeResult({ (result) in
                self.rac_product.value = result.value
            })
        }
        self.rac_price <~ self.rac_product.producer.skipNil().map({ (product) -> String in
            return "$ " + product.pricing!.price!
        })
        self.rac_title <~ self.rac_product.producer.skipNil().map({ (product) -> String in
            return product.title!
        })
        self.rac_desc <~ self.rac_product.producer.skipNil().map({ (product) -> String in
            return product.desc!
        })
        self.rac_cells <~ self.rac_product.producer.skipNil().map({ (product) -> Array<ProductCellViewModel> in
            return product.images!.map({ (img) -> ProductCellViewModel in
                return ProductCellViewModel(withModel: img)
            })
        })
        self.rac_itemCount <~ self.rac_product.producer.skipNil().map({ (product) -> Int in
            return product.images!.count
        })
        self.rac_loaded <~ self.rac_product.producer.skipNil().map({ (product) -> Bool in
            return true
        })
    }
    
    func viewModelForIndexPath( indexPath : IndexPath ) -> ProductCellViewModel {
        return self.rac_cells.value[indexPath.item]
    }
    
    deinit {
//        print( "ProductViewModel deinit" )
    }
}
