//
//  IndicatorView.swift
//  RMA
//
//  Created by 1002220 on 2016. 10. 11..
//  Copyright © 2016년 Jin. All rights reserved.
//

import UIKit

class IndicatorView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    fileprivate var indicatorView : UIActivityIndicatorView
    fileprivate var transparentView : UIView
    var running : Bool {
        set {
            if newValue == true {
                indicatorView.startAnimating()
            } else {
                indicatorView.stopAnimating()
            }
        }
        
        get {
            return indicatorView.isAnimating
        }
    }
    
    override init( frame : CGRect ) {
        indicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        transparentView = UIView()
        transparentView.backgroundColor = UIColor.black
        transparentView.alpha = 0.3
        transparentView.layer.cornerRadius = 8.0
        super.init(frame: frame)
        self.addSubview(transparentView)
        self.addSubview(indicatorView)
        
        self.backgroundColor = UIColor.clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var frame: CGRect {
        didSet {
            
            indicatorView.frame = CGRect(x: self.frame.size.width / 2 - 20.0 / 2, y: self.frame.size.height / 2  - 20.0 / 2, width: 20.0, height: 20.0)
            transparentView.frame = CGRect(origin: CGPoint(x: 0.0, y: 0.0), size: self.frame.size)
        }
    }
}
