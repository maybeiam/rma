//
//  HttpRequest.swift
//  RMA
//
//  Created by Shepherd on 2016. 10. 8..
//  Copyright © 2016년 Jin. All rights reserved.
//

import UIKit

class HttpRequest: NSObject {
    class func requestGet(
        _ baseUrl : String?,
        targetUrl : String,
        success : @escaping ( (NSDictionary) -> Void),
        failure : @escaping ( (NSError) -> Void),
        parametersObjectsAndKeys : Array<String> ) {
        
        var absoluteUrl : String?
        if baseUrl == nil {
            absoluteUrl = BASE_URL + "/" + API_VERSION
        }
        else {
            absoluteUrl = baseUrl
        }
        
        absoluteUrl! += targetUrl
        
        var parameters = [String]()
        var i = 0
        while i < parametersObjectsAndKeys.count {
            var string = parametersObjectsAndKeys[i + 1]
            string += "="
            string += parametersObjectsAndKeys[i]
            parameters.append(string)
            i += 2
        }
        
        absoluteUrl! += "?"
        absoluteUrl! += parameters.joined(separator: "&")
        let customAllowedSet =  CharacterSet(charactersIn:/*"=\"#%/<>?@\\^`{|}"*/" ").inverted
        absoluteUrl = absoluteUrl!.addingPercentEncoding(withAllowedCharacters: customAllowedSet)
        
        var request = URLRequest(url: URL(string: absoluteUrl!)!)
        request.httpMethod = "GET"
        
        HttpRequest.sendRequest(request, success: { (result) -> Void in
            success( result )
        }) { (error) -> Void in
            failure( error )
        }
        
    }
    
    class func requestImage( _ url : String, success : @escaping ( (UIImage) -> Void), failure : @escaping ( (Error) -> Void) ) {
        
        let session = URLSession.shared
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "GET"
        
        let task = session.dataTask(with: request) { (data : Data?, r : URLResponse?, error : Error? ) in
            if error == nil {
                if data == nil {
                    failure( NSError(domain: "data is nil", code: 302, userInfo: nil) )
                }
                else {
                    let image = UIImage(data: data!)
                    success( image! )
                }
            }
            else {
                failure( error! )
            }
        }
        task.resume()
    }
    
    fileprivate class func sendRequest( _ request : URLRequest, success : @escaping ( (NSDictionary) -> Void), failure : @escaping ( (NSError) -> Void) ) {
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (data : Data?, r : URLResponse?, error : Error? ) in
            if error == nil {
                if data == nil {
                    failure( NSError(domain: "data is nil", code: 302, userInfo: nil) )
                }
                else {
                    
                    let dictionary : NSDictionary = (try! JSONSerialization.jsonObject(with: data!, options : JSONSerialization.ReadingOptions.mutableContainers)) as! NSDictionary
                    print( "request for url : [\(request.url?.absoluteString)]\nits response : [\(dictionary)]")
                    success( dictionary )
                }
            }
            else {
            }
        }
        task.resume()
    }
    
    class func requestGet(
        _ baseUrl : String?,
        targetUrl : String,
        success : @escaping ( (NSDictionary) -> Void),
        failure : @escaping ( (NSError) -> Void),
        parametersObjectsAndKeys : String... ) {
        
        var absoluteUrl : String?
        if baseUrl == nil {
            absoluteUrl = BASE_URL + "/" + API_VERSION
        }
        else {
            absoluteUrl = baseUrl
        }
        
        absoluteUrl! += targetUrl
        
        var parameters = [String]()
        var i = 0
        while i < parametersObjectsAndKeys.count {
            var string = parametersObjectsAndKeys[i + 1]
            string += "="
            string += parametersObjectsAndKeys[i]
            parameters.append(string)
            i += 2
        }
        
        absoluteUrl! += "?"
        absoluteUrl! += parameters.joined(separator: "&")
        let customAllowedSet =  CharacterSet(charactersIn:/*"=\"#%/<>?@\\^`{|}"*/" ").inverted
        absoluteUrl = absoluteUrl!.addingPercentEncoding(withAllowedCharacters: customAllowedSet)
        
        var request = URLRequest(url: URL(string: absoluteUrl!)!)
        request.httpMethod = "GET"
        
        HttpRequest.sendRequest(request, success: { (result) -> Void in
            success( result )
        }) { (error) -> Void in
            failure( error )
        }
        
    }
}
