//
//  ProductViewController.swift
//  RMA
//
//  Created by 1002220 on 2016. 10. 10..
//  Copyright © 2016년 Jin. All rights reserved.
//

import UIKit
import QuartzCore
import ReactiveSwift
import Result

class ProductViewController: BaseViewController {
    
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var mainCollectionView: UICollectionView!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var scrollIndicatorView: UIView!
    @IBOutlet weak var saleView: UIView!
    @IBOutlet weak var saleLabel: UILabel!
    var pageIndicator : ScrollIndicatorView?
    
    var viewModel : ProductViewModel?
    override var loaded: Bool {
        didSet {
            self.mainCollectionView.reloadData()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.delegate = self
        self.navigationController?.isNavigationBarHidden = true
        self.view.backgroundColor = UIColor.clear
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initView()
        self.bindViewModel()
        // Do any additional setup after loading the view.
    }
    
    func initView() {
        mainScrollView.layer.cornerRadius = 10.0
        mainScrollView.layer.masksToBounds = true
        self.navigationController?.delegate = self
        mainCollectionView.register(UINib(nibName: "ProductCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ProductCollectionViewCell" )
        self.automaticallyAdjustsScrollViewInsets = false
        self.saleView.layer.cornerRadius = 4.0
        let layout = mainCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: SCREEN_SIZE.width - 40.0, height: ((SCREEN_SIZE.width - 40.0) * 0.9) )
    }
    
    func bindViewModel() {
        viewModel!.bind()
        priceLabel.rac_text <~ self.viewModel!.rac_price.producer.observe(on: UIScheduler())
        titleLabel.rac_text <~ self.viewModel!.rac_title.producer.observe(on: UIScheduler())
        descLabel.rac_text <~ self.viewModel!.rac_desc.producer.observe(on: UIScheduler())
        self.rac_loaded <~ self.viewModel!.rac_loaded.producer.observe(on: UIScheduler()).filter({ (loaded) -> Bool in
            return loaded == true
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonAction(_ sender: AnyObject) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ProductViewController : UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (viewModel?.imageCount)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCollectionViewCell", for: indexPath) as! ProductCollectionViewCell
        cell.viewModel = self.viewModel?.viewModelForIndexPath(indexPath: indexPath )
        (cell.viewModel as! ProductCellViewModel).startDownloadImage(until: cell.prepareForReuseSignal())
        return cell
    }
}

extension ProductViewController : UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        if operation == UINavigationControllerOperation.push {
            return PushViewControllerAnimation()
        } else if operation == UINavigationControllerOperation.pop {
            return PopViewControllerAnimation()
        } else {
            return nil
        }
    }
}
