//
//  MainCellViewModel.swift
//  RMA
//
//  Created by 1002220 on 2016. 10. 21..
//  Copyright © 2016년 Jin. All rights reserved.
//

import UIKit
import ReactiveSwift
import Result

class MainCellViewModel : ViewModelForCellWithOneImage {
    let rac_title = MutableProperty<String?>("")
    let rac_price = MutableProperty<String?>("")
    
    required init( withModel model : MODEL ) {
        super.init( withModel : model )
        let product = model as! PRODUCT
        self.url = (product.img?.imageUrl)!
    }
}
