//
//  LIST.swift
//  RMA
//
//  Created by Shepherd on 2016. 10. 8..
//  Copyright © 2016년 Jin. All rights reserved.
//

import UIKit
import ReactiveSwift
import Result

enum FetchError : Error {
    case UnkownError
    case APIError
    case NoError
}

protocol ListProtocol {
    associatedtype T : ModelProtocol
    var page : Int { get }
    var page_size : Int { get set }
    var items : Array<T> { get set }
    var rac_items : MutableProperty<Array<T>> { get }
    var itemCount : Int { get }
    var hasMore : Bool { get }
    init( withDictionary dictionary : NSDictionary )
}
