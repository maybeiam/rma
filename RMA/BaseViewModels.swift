//
//  BaseViewModel.swift
//  RMA
//
//  Created by Shepherd on 2016. 10. 8..
//  Copyright © 2016년 Jin. All rights reserved.
//

import UIKit
import ReactiveSwift
import Result

class BaseViewModel: NSObject {
    var name : String?
}

protocol ViewModelForCellProtocol {
    
}

protocol ViewModelForCellWithOneImageProtocol : ViewModelForCellProtocol {
    var rac_img : MutableProperty<UIImage?> { get }
    var url : String? { get set }
    
    init( withModel model : MODEL )
}

class ViewModelForCellWithOneImage : ViewModelForCellWithOneImageProtocol {
    let rac_img = MutableProperty<UIImage?>(nil)
    var url : String?
    
    required init( withModel model : MODEL ) {
        
    }
    
    public func startDownloadImage( until : Signal<Void,NoError> ) {
        if self.rac_img.value != nil {
            return
        }
        self.fetchImageDownloadSignal(url: self.url!).take(until: until)
            .startWithSignal{ (signal, disposable) in
                signal.observeResult({ [unowned self] (result) in
                    self.rac_img.value = result.value!
                    })
                signal.observeFailed({ (error) in
                    self.rac_img.value = nil
                })
        }
        
    }
    
    fileprivate func fetchImageDownloadSignal( url : String ) -> SignalProducer<UIImage?,NSError> {
        return SignalProducer { [unowned self] observer, disposable in
            
            observer.send(value: nil)
            
            let session = URLSession.shared
            let request = URLRequest(url: URL(string : url )! )
            let task = session.dataTask(with: request, completionHandler: { [unowned self] (data, response, error) in
                guard let image = UIImage(data: data!) else {
                    observer.send(error: NSError())
                    return
                }
                self.rac_img.value = image
                observer.send(value: self.rac_img.value)
                observer.sendCompleted()
                })
            task.resume()
        }
    }
}
