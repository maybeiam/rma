//
//  BaseCells.swift
//  RMA
//
//  Created by 1002220 on 2016. 10. 27..
//  Copyright © 2016년 Jin. All rights reserved.
//

import UIKit

protocol CustomCollectionViewCell {
    var viewModel : ViewModelForCellProtocol? { get set }
}
