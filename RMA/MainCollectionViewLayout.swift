//
//  MainCollectionViewLayout.swift
//  RMA
//
//  Created by Shepherd on 2016. 10. 9..
//  Copyright © 2016년 Jin. All rights reserved.
//

import UIKit

protocol MainLayoutDelegate : NSObjectProtocol {
    func collectionView( collectionView : UICollectionView, heightForItem indexPath : IndexPath, width : CGFloat ) -> CGFloat?
    
}

class MainCollectionViewLayout: UICollectionViewLayout {
    private var layoutInfo : Dictionary<IndexPath,UICollectionViewLayoutAttributes>?
    private var contentHeight = CGFloat(0.0)
    private var contentWidth : CGFloat {
        return (self.collectionView!.bounds.size.width - (self.collectionView!.contentInset.left + self.collectionView!.contentInset.right))
    }
    private let numberOfColumns = 2
    private let cellPadding = CGFloat(6.0)
    
    private func calculateEachItem() {
        layoutInfo = Dictionary<IndexPath,UICollectionViewLayoutAttributes>()
        let widthForEachColumn = (contentWidth - (CGFloat(numberOfColumns - 1) * cellPadding)) / CGFloat(numberOfColumns)
        var xOffset = Array<CGFloat>()
        for column in 0 ..< numberOfColumns {
            xOffset.append( CGFloat(column) * (widthForEachColumn + cellPadding) )
        }
        
        var yOffset = Array<CGFloat>(repeating: 0.0, count: numberOfColumns )
        
        for index in 0 ..< self.collectionView!.numberOfItems(inSection: 0) {
            
            let column = index % numberOfColumns
            let indexPath = IndexPath(item: index, section: 0)
            let width = widthForEachColumn
            let height = (delegate?.collectionView(collectionView: self.collectionView!, heightForItem: indexPath, width : width))!
            let frame = CGRect(x: xOffset[column], y: yOffset[column], width: width, height: height)
            let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            attributes.frame = frame
            yOffset[column] += height
            contentHeight = max( contentHeight, frame.origin.y + frame.size.height )
            layoutInfo?[indexPath] = attributes
            
        }
        
    }
    
    weak var delegate : MainLayoutDelegate?
    
    override func prepare() {
        self.calculateEachItem()
    }
    
    override func layoutAttributesForElements(in rect: CGRect) ->
        [UICollectionViewLayoutAttributes]? {
            var allAttributes = Array<UICollectionViewLayoutAttributes>()
            for (_, layout) in self.layoutInfo! {
                if layout.frame.intersects(rect) == true {
                    allAttributes.append(layout)
                }
            }
            return allAttributes
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return self.layoutInfo?[indexPath]
    }
    
    override var collectionViewContentSize: CGSize {
        return CGSize(width: contentWidth, height: contentHeight )
    }
}
