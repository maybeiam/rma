//
//  ViewController.swift
//  RMA
//
//  Created by Shepherd on 2016. 10. 8..
//  Copyright © 2016년 Jin. All rights reserved.
//

import UIKit
import ReactiveSwift
import Result

class MainViewController: BaseViewController {

    @IBOutlet weak var mainCollectionView: UICollectionView!
    var defaultCell : MainCollectionViewCell?
    var viewModel : MainViewModel?
    var defaultLabel = UILabel()
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.delegate = self
    }
    
    override var loaded: Bool {
        didSet {
//            self.mainCollectionView.reloadData()
        }
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initView()
        self.bindViewModel()
    }
    

    func initView() {
        defaultCell = Bundle.main.loadNibNamed("MainCollectionViewCell", owner: self, options: nil)?[0] as? MainCollectionViewCell
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        mainCollectionView.register(UINib(nibName: "MainCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "MainCollectionViewCell")
        mainCollectionView.contentInset = UIEdgeInsets(top: 0.0, left: 6.0, bottom: 0.0, right: 6.0)
        let layout = mainCollectionView.collectionViewLayout as? MainCollectionViewLayout
        layout?.delegate = self
    }
    
    func bindViewModel() {
        viewModel = MainViewModel()
        viewModel!.bind()
        viewModel!.loadSignal().observe(on: UIScheduler()).startWithSignal { (signal, disposable) in
            signal.observeCompleted {
                self.mainCollectionView.reloadData()
            }
        }

        self.indicatorView!.rac_running <~ self.viewModel!.rac_loading.producer.observe(on: UIScheduler()).map({ (newValue) -> Bool in
            return newValue
        })
        self.rac_loaded <~ self.viewModel!.rac_loading.producer.observe(on: UIScheduler()).filter({ (loaded) -> Bool in
            return loaded == false
        })
        self.rac_refreshControl <~ self.viewModel!.rac_isloadmore.producer.observe(on: UIScheduler()).filter({ (loaded) -> Bool in
            return true
        })
        self.mainCollectionView.rac_contentOffset
            .producer
            .skip(until: (self.viewModel?.loadedSignal())!)
            .filter({ (contentOffset) -> Bool in
                return contentOffset.y >= self.mainCollectionView.contentSize.height - self.mainCollectionView.bounds.size.height - 1.0
            })
            .filter({ (contentOffset) -> Bool in
                return (self.viewModel?.rac_hasmore.value)!
            })
            .filter({ (contentOffset) -> Bool in
                return self.viewModel!.rac_isloadmore.value == false
            })
            .map{ _ in () }
            .startWithSignal { (signal, disposable) in
                signal.observeValues {
                    (self.viewModel?.loadMoreSignal())!
                        .observe(on: UIScheduler())
                    .startWithSignal({ (signal, disposable) in
                        self.mainCollectionView.reloadData()
                    })
                }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension MainViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print( "itemcount : [\((viewModel?.itemCount)!)]")
        return (viewModel?.itemCount)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MainCollectionViewCell", for: indexPath ) as! MainCollectionViewCell
        let vm = viewModel?.viewModelForIndexPath(atIndexPath: indexPath)
        let signal = cell.prepareForReuseSignal()
        cell.bindViewModel(viewModel: vm, until: signal)
        (cell.viewModel as! MainCellViewModel).startDownloadImage(until: signal)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let productId = self.viewModel?.productId( atIndex: indexPath.item )
        let viewController = ProductViewController(nibName: "ProductViewController", bundle: nil)
        viewController.viewModel = ProductViewModel(withProductId: productId!)
        let attributes = collectionView.layoutAttributesForItem(at: indexPath)
        let frame = attributes?.frame
        let cell = collectionView.cellForItem(at: indexPath) as! MainCollectionViewCell
        self.transitionSource = ( cell : cell, coordinate : collectionView.convert(frame!, to: self.view), imageView : cell.mainImageView )
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension MainViewController : MainLayoutDelegate {
    func collectionView(collectionView: UICollectionView, heightForItem indexPath: IndexPath, width: CGFloat) -> CGFloat? {
        
        let vm = viewModel?.viewModelForIndexPath(atIndexPath: indexPath)
        
        let topPadding = 6.0
        let imageViewHeight = (width - 12.0) * 0.9
        let paddingBetweenImageView = 4.0
        defaultLabel.font = defaultCell?.nameLabel.font
        defaultLabel.text = vm?.rac_title.value
        defaultLabel.numberOfLines = 0
        let titleLabelHeight = defaultLabel.sizeThatFits(CGSize(width: width - 8.0, height: CGFloat.greatestFiniteMagnitude )).height
        defaultLabel.font = defaultCell?.priceLabel.font
        defaultLabel.text = vm?.rac_price.value
        defaultLabel.numberOfLines = 1
        let labelPadding = 2.0
        let priceLabelHeight = defaultLabel.sizeThatFits(CGSize(width: width - 8.0, height: CGFloat.greatestFiniteMagnitude )).height
        let lastPadding = 8.0
        
        return CGFloat(topPadding) + imageViewHeight + CGFloat(paddingBetweenImageView) + titleLabelHeight + CGFloat(labelPadding) + CGFloat(priceLabelHeight) + CGFloat(lastPadding)
    }
}

extension MainViewController : UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        if operation == UINavigationControllerOperation.push {
            return PushViewControllerAnimation()
        } else if operation == UINavigationControllerOperation.pop {
            return PopViewControllerAnimation()
        } else {
            return nil
        }
    }
}
