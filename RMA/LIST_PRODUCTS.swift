//
//  LIST_PRODUCTS.swift
//  RMA
//
//  Created by 1002220 on 2016. 10. 20..
//  Copyright © 2016년 Jin. All rights reserved.
//

import UIKit
import ReactiveSwift

class LIST_PRODUCTS<T : ModelProtocol> : ListProtocol {
    var page : Int {
        if self.itemCount % page_size == 0 && self.itemCount > 0 {
            return self.itemCount / (page_size == 0 ? 1 : page_size) - 1
        } else {
            return self.itemCount / (page_size == 0 ? 1 : page_size)
        }

    }
    var page_size : Int
    var itemCount : Int {
        return items.count
    }
    var hasMore : Bool {
        return (self.itemCount - (page + 1) * page_size) < 0 ? false : true
    }
    var items : Array<T>
    var rac_items = MutableProperty( Array<T>() )
    
    required init( withDictionary dictionary : NSDictionary ) {
        self.page_size = ((dictionary.object( forKey: "page_size" ) as? NSNumber)?.intValue)!
        let array = dictionary.object(forKey: "products") as! NSArray
        self.items = array.map({ (value) -> T in
            let item = value as! NSDictionary
            return PRODUCT(withDictionary: item) as! T
        })
    }
    
    static func downloadFirstpageProductListsFromServer() -> SignalProducer< LIST_PRODUCTS<T>, FetchError > {
        return SignalProducer { observer, disposable in
            HttpRequest.requestGet(nil, targetUrl: API_CATALOG, success: { (jsonObject) in
                let result = LIST_PRODUCTS(withDictionary: jsonObject)
                observer.send(value: result )
                observer.sendCompleted()
                }, failure: { (error) in
                    observer.send(error: FetchError.APIError)
                }, parametersObjectsAndKeys: "all-sales", "theme", "30", "pageSize", "0", "page")
        }
    }
    
    func downloadMorepageProductListsFromServer() -> SignalProducer< LIST_PRODUCTS<T>, FetchError > {
        return SignalProducer { observer, disposable in
            HttpRequest.requestGet(nil, targetUrl: API_CATALOG, success: { (jsonObject) in
                let result = LIST_PRODUCTS(withDictionary: jsonObject)
                    observer.send(value: result )
                    observer.sendCompleted()
                }, failure: { (error) in
                    observer.send(error: FetchError.APIError)
                }, parametersObjectsAndKeys: "all-sales", "theme", "30", "pageSize", String( self.page + 1 ), "page")
        }
    }
}
