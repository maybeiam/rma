//
//  PRODUCT_PRODUCTDETAIL.swift
//  RMA
//
//  Created by 1002220 on 2016. 10. 24..
//  Copyright © 2016년 Jin. All rights reserved.
//

import UIKit
import ReactiveSwift
import Result

class PRODUCT_PRODUCTDETAIL: PRODUCT {
    static func downloadProductDetailFromServer( productId : String ) -> SignalProducer< PRODUCT, FetchError > {
        return SignalProducer { observer, disposable in
            HttpRequest.requestGet(nil, targetUrl: API_PRODUCTDETAILS + "/" + productId, success: { (jsonObject) in
                let dictionary = jsonObject
                let dic = dictionary.object(forKey: "product") as! NSDictionary
                let result = PRODUCT(withDictionary: dic )
                observer.send(value: result )
                observer.sendCompleted()
                }, failure: { (error) in
                    observer.send(error: FetchError.APIError)
                })
        }
    }
}
