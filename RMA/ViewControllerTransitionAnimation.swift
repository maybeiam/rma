//
//  ViewControllerTransitionAnimation.swift
//  RMA
//
//  Created by 1002220 on 2016. 10. 11..
//  Copyright © 2016년 Jin. All rights reserved.
//

import UIKit

class PopViewControllerAnimation : NSObject, UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to ) as? BaseViewController
        if toViewController?.transitionSource == nil {
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            return
        }
        let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from )
        let container = transitionContext.containerView
        
        container.addSubview((toViewController?.view)!)
        
        let moveView = UIView()
        let moveImage = fromViewController?.view.screenshot
        let imageView = UIImageView(image: moveImage)
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        imageView.frame = CGRect(x: 0.0, y: 0.0, width: SCREEN_SIZE.width, height: SCREEN_SIZE.height )
        moveView.frame = CGRect(x: 0.0, y: 0.0, width: SCREEN_SIZE.width, height: SCREEN_SIZE.height )
        container.addSubview(imageView)
        
        UIView.animate(withDuration: self.transitionDuration(using: transitionContext), delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 0.3, options: [], animations: { () -> Void in
            
            imageView.frame = (toViewController?.transitionSource?.coordinate)!
            imageView.alpha = 0.0
            
        }) { _ in
            imageView.removeFromSuperview()
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            
        }
    }
}

class PushViewControllerAnimation : NSObject, UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from ) as? BaseViewController
        if fromViewController?.transitionSource == nil {
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            return
        }
        let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to )
        let container = transitionContext.containerView
        
        container.addSubview((toViewController?.view)!)
        
        let fromView = UIView()
        fromView.frame = (fromViewController?.transitionSource?.coordinate)!
        fromView.backgroundColor = UIColor.black
        let insideView = UIView()
        let hMargin = 20.0 * (fromView.frame.size.width / UIScreen.main.bounds.width)
        let vMargin = 20.0 * (fromView.frame.size.height / UIScreen.main.bounds.height)
        insideView.frame = CGRect(x: hMargin, y: vMargin, width: fromView.frame.size.width - hMargin * 2, height: fromView.frame.size.height - vMargin * 2)
        fromView.addSubview(insideView)
        insideView.backgroundColor = UIColor.white
        insideView.layer.cornerRadius = 10.0
        container.addSubview(fromView)
        
        let image = fromViewController?.transitionSource?.imageView.screenshot
        let imageView = UIImageView(image: image)
        imageView.frame = CGRect(x: fromView.frame.origin.x + 6.0, y: fromView.frame.origin.y + 6.0, width: (fromView.frame.size.width - 12.0), height: (fromView.frame.size.width - 12.0) * 0.9 )
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        container.addSubview(imageView)
        
        let collectionViewSize = SCREEN_SIZE.width - 40.0
        let collectionViewHeight = collectionViewSize * 0.9
        
        toViewController?.view.isHidden = true
        
        UIView.animate(withDuration: self.transitionDuration(using: transitionContext), delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 0.3, options: [], animations: { () -> Void in
            fromView.frame = CGRect(x: 0.0, y: 0.0, width: SCREEN_SIZE.width, height: SCREEN_SIZE.height)
            insideView.frame = CGRect(x: 20.0, y: 20.0, width: SCREEN_SIZE.width - 40.0, height: SCREEN_SIZE.height - 40.0)
            imageView.frame = CGRect(x: SCREEN_SIZE.width / 2 - (collectionViewSize * 0.7) / 2, y: (20.0 + collectionViewHeight) - 10.0 - (collectionViewSize * 0.7 * 0.9), width: collectionViewSize * 0.7, height: collectionViewHeight * 0.7 )

        }) { _ in
            fromView.removeFromSuperview()
            imageView.removeFromSuperview()
            toViewController?.view.isHidden = false
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            
        }
    }
}
