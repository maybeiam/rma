//
//  IMG.swift
//  RMA
//
//  Created by Shepherd on 2016. 10. 8..
//  Copyright © 2016년 Jin. All rights reserved.
//

import UIKit
import ReactiveSwift
import Result

class IMG: MODEL {
    var h : CGFloat?
    var w : CGFloat?
    var name : String?
    var position : Int?
    var imageUrl : String? {
        return name == nil ? "" : (IMAGESERVER_URL + name!)
    }
    
    init( withDictionary dictionary : NSDictionary ) {
        name = dictionary.object(forKey: "name") as? String
        position = ((dictionary.object(forKey: "position") as? NSString)?.integerValue)
        if dictionary.object(forKey: "w") is NSNumber {
            w = CGFloat(((dictionary.object(forKey: "w") as? NSNumber)?.intValue)!)
        } else if dictionary.object(forKey: "w") is String {
            w = CGFloat(((dictionary.object(forKey: "w") as? NSString)?.intValue)!)
        }
        
        if dictionary.object(forKey: "h") is NSNumber {
            h = CGFloat(((dictionary.object(forKey: "h") as? NSNumber)?.intValue)!)
        } else if dictionary.object(forKey: "w") is String {
            h = CGFloat(((dictionary.object(forKey: "h") as? NSString)?.intValue)!)
        }
        
        
    }
    
    static func requestImageDownloadFromServer( url : String ) -> SignalProducer< UIImage, FetchError > {
        return SignalProducer { observer, disposable in
            let session = URLSession.shared
            let request = URLRequest(url: URL(string : url )! )
            let task = session.dataTask(with: request, completionHandler: { (data, response, error) in
                guard let image = UIImage(data: data!) else {
                    observer.send(error: .APIError )
                    return
                }
                observer.send(value: image)
                observer.sendCompleted()
                })
            task.resume()
        }
    }
}
