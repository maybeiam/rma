//
//  PRODUCT.swift
//  RMA
//
//  Created by Shepherd on 2016. 10. 8..
//  Copyright © 2016년 Jin. All rights reserved.
//

import UIKit

class PRODUCT: MODEL {
    let title : String?
    let img : IMG?
    let desc : String?
    let pricing : PRICING?
    let id : String?
    let images : Array<IMG>?
    
    init( withDictionary dictionary : NSDictionary ) {
        title = dictionary.object(forKey: "title") as? String
        id = (dictionary.object(forKey: "id") as? NSNumber)?.stringValue
        img = IMG(withDictionary: dictionary.object(forKey: "img") as! NSDictionary)
        desc = dictionary.object(forKey: "desc") as? String
        pricing = PRICING(withDictionary: dictionary.object(forKey: "pricing") as! NSDictionary)
        images = Array<IMG>()
        let imageArray = dictionary.object(forKey: "images") as? NSArray
        if imageArray != nil {
            for object in imageArray! {
                let image = object as? NSDictionary
                images?.append(IMG(withDictionary: image!))
            }
        }
    }
}
