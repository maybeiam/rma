//
//  CustomUICollectionViewCell.swift
//  RMA
//
//  Created by 1002220 on 2016. 10. 21..
//  Copyright © 2016년 Jin. All rights reserved.
//

import UIKit

protocol CollectionViewCellProtocol {
    var viewModel : ViewModelForCellProtocol { set get }
}
