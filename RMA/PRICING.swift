//
//  PRICING.swift
//  RMA
//
//  Created by Shepherd on 2016. 10. 8..
//  Copyright © 2016년 Jin. All rights reserved.
//

import UIKit

class PRICING: MODEL {
    var on_sale : Bool?
    var price : String?
    var promo_price : String?
    var savings : String?
    var savings_type : String?
    var savings_text : String?
    
    init( withDictionary dictionary : NSDictionary ) {
        on_sale = ((dictionary.object(forKey: "on_sale") as? NSNumber)?.isEqual(to: NSNumber(value: 0)))! ? false : true
        price = (dictionary.object(forKey: "price") as? NSNumber)?.stringValue
        promo_price = (dictionary.object(forKey: "promo_price") as? NSNumber)?.stringValue
        savings_text = dictionary.object(forKey: "savings_text") as? String
    }
}
