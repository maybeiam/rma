//
//  MainViewModelForCell.swift
//  RMA
//
//  Created by 1002220 on 2016. 10. 21..
//  Copyright © 2016년 Jin. All rights reserved.
//

import UIKit
import ReactiveSwift
import Result

class MainViewModelForCell<T : ModelProtocol> : BaseViewModelForCellWithOneImage<T> {
    let rac_title = MutableProperty<String?>("")
    let rac_price = MutableProperty<String?>("")
    let rac_product = MutableProperty<T?>(nil)
    
    required init<T : ModelProtocol>( withModel model : T ) {
        super.init( withModel : model )
        let product = model as! PRODUCT
        self.rac_product.value = product
        self.url = product.img?.imageUrl
    }
}
