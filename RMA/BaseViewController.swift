//
//  BaseViewController.swift
//  RMA
//
//  Created by Shepherd on 2016. 10. 8..
//  Copyright © 2016년 Jin. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    var transitionSource : ( cell : UICollectionViewCell, coordinate : CGRect, imageView : UIImageView )?
    var loaded = false
    var indicatorView : IndicatorView?
    var refreshControl = false

    override func viewDidLoad() {
        super.viewDidLoad()
        indicatorView = IndicatorView()
        indicatorView?.isHidden = true
        indicatorView?.running = false
        self.view.addSubview(indicatorView!)
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        indicatorView?.frame = CGRect(x: SCREEN_SIZE.width / 2 - 60.0 / 2, y: SCREEN_SIZE.height / 2 - 60.0 / 2, width: 60.0, height: 60.0 )
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func runIndicator() {
        self.view?.bringSubview(toFront: self.indicatorView!)
        self.indicatorView?.isHidden = false
        self.indicatorView?.running = true
    }
    
    func stopIndicator() {
        self.indicatorView?.isHidden = true
        self.indicatorView?.running = false
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
