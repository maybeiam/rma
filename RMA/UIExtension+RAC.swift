//
//  UIExtension+RAC.swift
//  RMA
//
//  Created by 1002220 on 2016. 10. 20..
//  Copyright © 2016년 Jin. All rights reserved.
//

import UIKit
import ReactiveSwift
import Result

struct AssociatedKey {
    static var contentOffset : UInt8 = 1
    static var prepareForReuse : UInt8 = 2
    static var text : UInt8 = 3
    static var loaded : UInt8 = 4
    static var collectionViewDataSource : UInt8 = 5
    static var running : UInt8 = 6
    static var refreshControl : UInt8 = 6
    static var prepareForReuseSignal : UInt8 = 7
    static var prepareForReuseObserver : UInt8 = 8
}

func synchronize( key :  AnyObject, closure : () -> Void ) {
    objc_sync_enter(key)
    closure()
    objc_sync_exit(key)
}

extension UILabel {
    public var rac_text : MutableProperty<String?> {
        get {
            if objc_getAssociatedObject(self, &AssociatedKey.text) == nil {
                synchronize(key: self ) {
                    let property = MutableProperty(self.text)
                    property.producer.startWithValues { newValue in
                        self.text = newValue
                    }
                    objc_setAssociatedObject(self, &AssociatedKey.text, property, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
                }
            }
            return objc_getAssociatedObject(self, &AssociatedKey.text) as! MutableProperty<String?>
        }
    }
}

extension UIImageView {
    public var rac_image : MutableProperty<UIImage?> {
        get {
            if objc_getAssociatedObject(self, &AssociatedKey.text) == nil {
                let property = MutableProperty(self.image)
                property.producer.startWithValues { newValue in
                    self.image = newValue
                }
                objc_setAssociatedObject(self, &AssociatedKey.text, property, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
            }
            return objc_getAssociatedObject(self, &AssociatedKey.text) as! MutableProperty<UIImage?>
        }
    }
}

extension UICollectionView {
    
    var rac_contentOffset : MutableProperty<CGPoint> {
        get {
            if objc_getAssociatedObject(self, &AssociatedKey.contentOffset) == nil {
                let property = MutableProperty(self.contentOffset)
                objc_setAssociatedObject(self, &AssociatedKey.contentOffset, property, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
            }
            return objc_getAssociatedObject(self, &AssociatedKey.contentOffset) as! MutableProperty<CGPoint>
        }
    }

    var rac_collectionViewDataSource : SignalProducer<Void, NSError>? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKey.collectionViewDataSource) as? SignalProducer<Void, NSError>
        }
        set {
            objc_setAssociatedObject(self, &AssociatedKey.collectionViewDataSource, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    override open func setContentOffset(_ contentOffset: CGPoint, animated: Bool) {
        self.rac_contentOffset.value = self.contentOffset
        super.setContentOffset(contentOffset, animated: animated)
    }
    
    override open var contentOffset: CGPoint {
        didSet {
            self.rac_contentOffset.value = self.contentOffset
        }
    }
}

extension IndicatorView {
    var rac_running : MutableProperty<Bool> {
        get {
            if objc_getAssociatedObject(self, &AssociatedKey.running) == nil {
                let property = MutableProperty(false)
                property.producer.startWithValues{ newValue in
                    self.isHidden = !newValue
                    self.running = newValue
                }
                objc_setAssociatedObject(self, &AssociatedKey.running, property, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
            }
            return objc_getAssociatedObject(self, &AssociatedKey.running) as! MutableProperty<Bool>
        }
    }
}
extension UICollectionViewCell {
    
    fileprivate var rac_prepareForReuseSignal : Signal<Void,NoError> {
        get {
                if objc_getAssociatedObject(self, &AssociatedKey.prepareForReuseSignal) == nil {
                    synchronize(key: self) {
                        let pair = Signal<Void,NoError>.pipe()
                        objc_setAssociatedObject(self, &AssociatedKey.prepareForReuseSignal, pair.0, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
                        objc_setAssociatedObject(self, &AssociatedKey.prepareForReuseObserver, pair.1, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
                    }
                }
            return objc_getAssociatedObject(self, &AssociatedKey.prepareForReuseSignal) as! Signal<Void,NoError>
        }
    }
    
    fileprivate var rac_prepareForReuseObserver : Observer<Void,NoError> {
        get {
            if objc_getAssociatedObject(self, &AssociatedKey.prepareForReuseObserver) == nil {
                synchronize(key: self) {
                    let pair = Signal<Void,NoError>.pipe()
                    objc_setAssociatedObject(self, &AssociatedKey.prepareForReuseSignal, pair.0, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
                    objc_setAssociatedObject(self, &AssociatedKey.prepareForReuseObserver, pair.1, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
                }
            }
            return objc_getAssociatedObject(self, &AssociatedKey.prepareForReuseObserver) as! Observer<Void,NoError>
        }
    }
    
    func prepareForReuseSignal() -> Signal< Void, NoError > {
        return self.rac_prepareForReuseSignal
    }
    
    override open func prepareForReuse() {
        self.rac_prepareForReuseObserver.sendCompleted()
        
        super.prepareForReuse()
    }
}

extension BaseViewController {
    public var rac_loaded : MutableProperty<Bool> {
        get {
            if objc_getAssociatedObject(self, &AssociatedKey.loaded) == nil {
                let property = MutableProperty(self.loaded)
                property.producer.startWithValues { newValue in
                    self.loaded = newValue
                }
                objc_setAssociatedObject(self, &AssociatedKey.loaded, property, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
            }
            return objc_getAssociatedObject(self, &AssociatedKey.loaded) as! MutableProperty<Bool>
        }
    }
    
    public var rac_refreshControl : MutableProperty<Bool> {
        get {
            if objc_getAssociatedObject(self, &AssociatedKey.refreshControl) == nil {
                let property = MutableProperty(self.refreshControl)
                property.producer.startWithValues { newValue in
                    self.refreshControl = newValue
                }
                objc_setAssociatedObject(self, &AssociatedKey.refreshControl, property, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
            }
            return objc_getAssociatedObject(self, &AssociatedKey.refreshControl) as! MutableProperty<Bool>
        }
    }
}
