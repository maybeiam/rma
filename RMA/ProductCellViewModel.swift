//
//  ProductCellViewModel.swift
//  RMA
//
//  Created by 1002220 on 2016. 10. 24..
//  Copyright © 2016년 Jin. All rights reserved.
//

import UIKit
import ReactiveSwift
import Result

class ProductCellViewModel : ViewModelForCellWithOneImage {
    var img : UIImage?
    required init( withModel model : MODEL ) {
        super.init( withModel : model )
        let img = model as! IMG
        self.url = (img.imageUrl)!
    }
//    
//    public func startDownloadImage( until : Signal< Void, NoError > ) {
//        self.fetchImageDownloadSignal(url: self.url).take(until: until).startWithSignal { (signal, disposable) in
//            signal.observeResult({ [unowned self] (result) in
//                self.rac_img.value = result.value
//                })
//        }
//    }
//    
//    fileprivate func fetchImageDownloadSignal( url : String ) -> SignalProducer<UIImage,NSError> {
//        
//        return SignalProducer { [unowned self] observer, disposable in
//            if self.img != nil {
//                observer.send(value: self.img!)
//                observer.sendCompleted()
//            } else {
//                let session = URLSession.shared
//                let request = URLRequest(url: URL(string : url )! )
//                let task = session.dataTask(with: request, completionHandler: { [unowned self] (data, response, error) in
//                    guard let image = UIImage(data: data!) else {
//                        observer.send(error: NSError())
//                        return
//                    }
//                    self.img = image
//                    observer.send(value: image)
//                    observer.sendCompleted()
//                    })
//                task.resume()
//            }
//            
//        }
//    }
}
