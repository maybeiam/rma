//
//  ScrollIndicatorView.swift
//  RMA
//
//  Created by 1002220 on 2016. 10. 10..
//  Copyright © 2016년 Jin. All rights reserved.
//

import UIKit

class DotIndicatorView : UIView {
    fileprivate var _isAvtice = false
    var isActive : Bool {
        set {
            _isAvtice = newValue
            if _isAvtice == true {
                self.backgroundColor = UIColor.orange
            } else {
                self.backgroundColor = UIColor.gray
            }
        }
        get {
            return _isAvtice
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.layer.cornerRadius = 6.0
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class ScrollIndicatorView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    fileprivate var indicators = Dictionary<Int,DotIndicatorView>()
    fileprivate var _totalPage = 0
    fileprivate var _currentPage = 0
    
    var totalPage : Int {
        set {
            _totalPage = newValue
            self.reload()
        }
        get {
            return _totalPage
        }
    }
    
    var currentPage : Int {
        set {
            _currentPage = newValue
            for (key, value) in indicators {
                if key == _currentPage {
                    value.isActive = true
                } else {
                    value.isActive = false
                }
            }
        }
        get {
            return _currentPage
        }
    }
    
    func reload() {
        for (_, value) in indicators {
            value.removeFromSuperview()
        }
        self.indicators.removeAll()
        self.currentPage = 0
        
        let padding = 6.0
        let indicatorSize = 12.0
        
        self.frame = CGRect(x: self.frame.origin.x, y: self.frame.origin.y, width: CGFloat(indicatorSize * Double(self.totalPage) + padding * Double(self.totalPage)), height: CGFloat(indicatorSize))
        
        for position in 0 ..< self.totalPage {
            let view = DotIndicatorView(frame: CGRect(x: (padding + indicatorSize) * Double(position), y: 0.0, width: indicatorSize, height: indicatorSize))
            self.addSubview(view)
            if position == self.currentPage {
                view.isActive = true
            } else {
                view.isActive = false
            }
            indicators[position] = view
        }
    }
}
