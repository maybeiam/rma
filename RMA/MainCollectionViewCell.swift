//
//  MainCollectionViewCell.swift
//  RMA
//
//  Created by Shepherd on 2016. 10. 9..
//  Copyright © 2016년 Jin. All rights reserved.
//

import UIKit
import ReactiveSwift
import Result

class MainCollectionViewCell : UICollectionViewCell, CustomCollectionViewCell {

    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var imageViewWidth: NSLayoutConstraint!
    @IBOutlet weak var bgViewWidth: NSLayoutConstraint!
    
    var viewModel: ViewModelForCellProtocol?
    // {
//        didSet {
//            self.configureFromViewModel()
//        }
    //}
    
    open func bindViewModel( viewModel : ViewModelForCellProtocol?, until : Signal<Void,NoError> ) {
        let vm = viewModel as! MainCellViewModel
        self.viewModel = vm
        self.mainImageView.rac_image <~ vm.rac_img.producer.observe(on: UIScheduler()).take(until: until)
        self.nameLabel.rac_text <~ vm.rac_title.producer.skipNil().observe(on: UIScheduler()).take(until: until)
        self.priceLabel.rac_text <~ vm.rac_price.producer.skipNil().observe(on: UIScheduler()).take(until: until)
    }
    
    private func configureFromViewModel() {
        let vm = self.viewModel as! MainCellViewModel
        self.mainImageView.rac_image <~ vm.rac_img.producer.skipNil().observe(on: UIScheduler())
        self.nameLabel.rac_text <~ vm.rac_title.producer.skipNil().observe(on: UIScheduler())
        self.priceLabel.rac_text <~ vm.rac_price.producer.skipNil().observe(on: UIScheduler())
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        bgView.layer.borderWidth = 1.0
        bgView.layer.borderColor = UIColor.lightGray.cgColor
        bgView.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        bgView.layer.shadowColor = UIColor.darkGray.cgColor
        bgView.layer.shadowRadius = 0.5
        bgView.layer.shadowOpacity = 1.0
        bgView.layer.masksToBounds = false
    }

}
