//
//  ProductCollectionViewCell.swift
//  RMA
//
//  Created by 1002220 on 2016. 10. 10..
//  Copyright © 2016년 Jin. All rights reserved.
//

import UIKit
import ReactiveSwift
import Result

class ProductCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet fileprivate weak var indicatorView: UIActivityIndicatorView!
    
    var downloading : Bool {
        set {
            if newValue == true {
                indicatorView.startAnimating()
            } else {
                indicatorView.stopAnimating()
            }
        }
        get {
            return self.downloading
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    var viewModel: ViewModelForCellProtocol? {
        didSet {
            self.configureFromViewModel()
        }
    }
    
    private func configureFromViewModel() {
        let vm = self.viewModel as! ProductCellViewModel
        self.mainImageView.rac_image <~ vm.rac_img.producer.skipNil().observe(on: UIScheduler())
        
    }

}
