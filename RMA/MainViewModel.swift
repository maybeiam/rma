//
//  MainViewModel.swift
//  RMA
//
//  Created by Shepherd on 2016. 10. 8..
//  Copyright © 2016년 Jin. All rights reserved.
//

import UIKit
import ReactiveSwift
import Result

class MainViewModel: BaseViewModel {
    var itemCount : Int {
        return self.rac_cellVms.value.count
    }
    var isLoadingMore : Bool {
        return self.isLoadMore
    }
    fileprivate var isLoadMore = false
    fileprivate let rac_list_products = MutableProperty<LIST_PRODUCTS<PRODUCT>?>(nil)
    fileprivate let rac_list_products_items = MutableProperty<Array<PRODUCT>>(Array<PRODUCT>())
    fileprivate let rac_cellVms = MutableProperty<Array<MainCellViewModel>>(Array<MainCellViewModel>())
    let rac_loaded = MutableProperty<Bool>(false)
    let rac_loading = MutableProperty<Bool>(false)
    let rac_hasmore = MutableProperty<Bool>(false)
    let rac_isloadmore = MutableProperty<Bool>(false)
    
    func loadedSignal() -> Signal<Void,NoError> {

        return Signal<Void,NoError> { [unowned self] observer in
            self.rac_list_products.producer
            .skipNil()
            .skip(while: {(list) -> Bool in
                return list.itemCount <= 0
            })
            .map { _ in () }
            .startWithValues {
                observer.sendCompleted()
            }
        }
    }
    
    func refreshSignal() -> SignalProducer<Void, NoError> {
        return rac_list_products.producer.map{ _ in () }
    }
    
    func bind() {
        self.rac_cellVms <~ self.rac_list_products.producer.skipNil().map({ (list) -> Array<MainCellViewModel> in
            let originItems = self.rac_cellVms.value
            let newItems = list.items[self.rac_cellVms.value.count ... list.items.count - 1]
                .map({ (product) -> MainCellViewModel in
                    let vm = MainCellViewModel( withModel : product )
                    let rac_product = MutableProperty<PRODUCT>( product )
                    vm.rac_title <~ rac_product.producer.map({ (p) -> String? in
                        return p.title
                    })
                    vm.rac_price <~ rac_product.producer.map({ (p) -> String? in
                        return p.pricing?.price
                    })
                    return vm
                })
            return [originItems, newItems].flatMap{ $0 }
        })
        self.rac_hasmore <~ self.rac_list_products.producer.skipNil().map({ (products) -> Bool in
            return products.hasMore
        })
    }
    
    func load() {
        LIST_PRODUCTS<PRODUCT>.downloadFirstpageProductListsFromServer()
            .mapError{ _ in return FetchError.APIError }
            .startWithSignal { (signal, disposable) in
                self.rac_loading.value = true
                signal.observeResult({ (result) in
                    self.rac_list_products.value = result.value
                })
                signal.observeCompleted {
                    self.rac_loading.value = false
                }
        }
    }
    
    func loadSignal() -> SignalProducer< Void, FetchError > {
        return SignalProducer< Void, FetchError > { [unowned self] observer, disposable in
            LIST_PRODUCTS<PRODUCT>.downloadFirstpageProductListsFromServer()
                .mapError{ _ in return FetchError.APIError }
                .startWithSignal { (signal, disposable) in
                    self.rac_loading.value = true
                    signal.observeResult({ (result) in
                        self.rac_list_products.value = result.value
                    })
                    signal.observeCompleted {
                        self.rac_loading.value = false
                        observer.sendCompleted()
                    }
                    signal.observeFailed({ (error) in
                        observer.send(error: error)
                    })
            }
        }
        
    }
    
    func loadMoreSignal() -> SignalProducer< Void, NoError> {
//        if self.rac_isloadmore.value == true { return SignalProducer< Void, NoError >.empty }
        
        return SignalProducer< Void, NoError > { [unowned self] observer, disposable in
            self.rac_list_products.value?.downloadMorepageProductListsFromServer()
                .mapError{ _ in return FetchError.APIError }
                .filter({ (products) -> Bool in
                    return products.items.count > 0
                })
                .startWithSignal { [unowned self] (signal, disposable) in
                    self.rac_isloadmore.value = true
                    signal.observeResult({ (result) in
                        result.value!.items = [self.rac_list_products.value!.items, result.value!.items].flatMap{ $0 }
                        self.rac_list_products.value = result.value
                    })
                    signal.observeCompleted {
                        self.rac_isloadmore.value = false
                        observer.sendCompleted()
                    }
            }
        }
    }
    
    func viewModelForIndexPath( atIndexPath indexPath : IndexPath ) -> MainCellViewModel {
        return rac_cellVms.value[indexPath.item]
    }
    
    func productId( atIndex index : Int ) -> String? {
        let object = (rac_list_products.value?.items[index])! as PRODUCT
        return object.id
    }

    
}
