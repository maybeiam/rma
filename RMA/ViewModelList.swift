//
//  ViewModelList.swift
//  RMA
//
//  Created by Shepherd on 2016. 10. 8..
//  Copyright © 2016년 Jin. All rights reserved.
//

import UIKit



class ViewModelList<T> : NSObject {
    override init() {
        super.init()
        items = Array<T>()
    }
    
    init( withPageSize count : Int ) {
        pageSize = count
        items = Array<T>()
    }
    
    init( array : Array<T>, pagesize : Int ) {
        self.pageSize = pagesize
        items = array
    }
    
    var page : Int {
        if self.itemCount % pageSize == 0 && self.itemCount > 0 {
            return self.itemCount / (pageSize == 0 ? 1 : pageSize) - 1
        } else {
            return self.itemCount / (pageSize == 0 ? 1 : pageSize)
        }
        
    }
    var pageSize = 1
    var hasMore : Bool {
        return (self.itemCount - (page + 1) * pageSize) < 0 ? false : true
    }
    var itemCount : Int {
        return items == nil ? 0 : items!.count
    }
    var items : Array<T>?
    
    func append( item newElement : T ) {
        items?.append(newElement)
    }
    
    func item( atIndex index : Int ) -> T {
        return items![index]
    }
    
    deinit {
//        print( "ViewModelList deinit" )
    }
}
