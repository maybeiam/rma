//
//  MainViewModelTests.swift
//  RMA
//
//  Created by Shepherd on 2016. 10. 8..
//  Copyright © 2016년 Jin. All rights reserved.
//

import XCTest
@testable import RMA

//class MockModel {
//    var page = 0
//    var pageSize = 30
//}
//
//class MockViewModelList {
//    var page = 0
//    var pageSize = 0
//    convenience init( withMockModel model : MockModel ) {
//        self.init()
//        self.page = model.page
//        self.pageSize = model.pageSize
//    }
//}

class MainViewModelTests: XCTestCase {
    
    var viewModel : MainViewModel?
    
    override func setUp() {
        super.setUp()
        viewModel = MainViewModel()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        viewModel = nil
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testDataSourceCount() {
        XCTAssertTrue(viewModel?.itemCount == 0)
    }
    
//    func testInsertData() {
//        viewModel?.insertData()
//        XCTAssertTrue(viewModel?.itemCount == 1)
//    }
    
    func testLoadData() {
        viewModel?.loadData()
        sleep( 5 )
        XCTAssertTrue(viewModel?.itemCount == 30)
    }
    
    func testItemAtIndex() {
        viewModel?.loadData()
        sleep( 5 )
        XCTAssertNotNil(viewModel?.item( atIndex : 0 ))
    }
    
    func testItemAtFirstIndexHasTitle() {
        viewModel?.loadData()
        sleep( 5 )
        let item = viewModel?.item( atIndex : 0 )
        
        XCTAssertTrue( item?.responds(to: #selector( getter : Item.title )) == true)
    }
    
    func testItemAtFirstIndexHasAllRequirements() {
        viewModel?.loadData()
        sleep( 5 )
        let item = viewModel?.item( atIndex : 0 )
        
        XCTAssertTrue( item?.responds(to: #selector( getter : Item.title )) == true)
        XCTAssertTrue( item?.responds(to: #selector( getter : Item.img )) == true)
        XCTAssertTrue( item?.responds(to: #selector( getter : Item.price )) == true)
    }
    
    func testItemAtFirstIndexHasRightValue() {
        viewModel?.loadData()
        sleep( 5 )
        let item = viewModel?.item( atIndex : 0 )
        
        XCTAssertTrue( item?.title == "Persil Fiber Intelligent Low Suds Powder Detergent + Free 500G" )
        XCTAssertTrue( item!.price! == "14.75" )
    }
    
//    func testLoadMore() {
//        viewModel?.loadMore()
//    }
//    
    func testLoadMoreHasMoreThanThirtyElements() {
        viewModel?.loadData()
        sleep( 5 )
        viewModel?.loadMore()
        sleep( 5 )
        XCTAssertTrue((viewModel?.itemCount)! > 30)
    }
    
    func testLoadMorePauseFlag() {
        viewModel?.loadData()
        sleep( 5 )
        viewModel?.loadMore()
        XCTAssertTrue(viewModel?.isLoadingMore == true )
    }
    
    func testLoadMorePauseFlagBackToNormalStatus() {
        viewModel?.loadData()
        sleep( 5 )
        viewModel?.loadMore()
        XCTAssertTrue(viewModel?.isLoadingMore == true )
        sleep( 5 )
        XCTAssertTrue(viewModel?.isLoadingMore == false )
    }
    
    func testDownloadImageAtIndexPath() {
        viewModel?.loadData()
        sleep( 5 )
        let item = viewModel?.item(atIndex: 0)
        XCTAssertNil(item?.img)
        viewModel?.requestImageDownload(atIndexPath: IndexPath(item: 0, section: 0))
        sleep( 5 )
        XCTAssertNotNil(item?.img)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
