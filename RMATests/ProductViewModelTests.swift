//
//  ProductViewModelTests.swift
//  RMA
//
//  Created by 1002220 on 2016. 10. 10..
//  Copyright © 2016년 Jin. All rights reserved.
//

import XCTest
@testable import RMA

class ProductViewModelTests: XCTestCase {
    
    var viewModel : ProductViewModel?
    var viewModelForInitTest : ProductViewModel?
    var viewModelForTest : ProductViewModel?
    let pid = "51231"
    override func setUp() {
        super.setUp()
        viewModel = ProductViewModel()
        viewModelForTest = ProductViewModel(withProductId: pid)
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
//    func testProductId() {
//        XCTAssertNotNil(viewModel?.productId)
//    }
    
//    func testPrice() {
//        XCTAssertNotNil(viewModel?.price)
//    }
    
//    func testVariables() {
//        XCTAssertNotNil(viewModel?.productId)
//        XCTAssertNotNil(viewModel?.price)
//        XCTAssertNotNil(viewModel?.savingText)
//        XCTAssertNotNil(viewModel?.images)
//        XCTAssertNotNil(viewModel?.title)
//        XCTAssertNotNil(viewModel?.desc)
//        XCTAssertNotNil(viewModel?.promoPrice)
//    }
    
//    func testInit() {
//        viewModelForInitTest = ProductViewModel( withProductId : pid )
//        XCTAssertNotNil(viewModel?.productId)
//    }
    
    func testVariables() {
        viewModelForTest?.loadData()
        sleep(3)
        XCTAssertNotNil(viewModelForTest?.itemForProduct?.price)
        XCTAssertNotNil(viewModelForTest?.itemForProduct?.savingText)
        XCTAssertNotNil(viewModelForTest?.itemForProduct?.images)
        XCTAssertNotNil(viewModelForTest?.itemForProduct?.title)
        XCTAssertNotNil(viewModelForTest?.itemForProduct?.desc)
        XCTAssertNotNil(viewModelForTest?.itemForProduct?.promoPrice)
    }
    
    func testLoadData() {
        viewModelForTest?.loadData()
        sleep(3)
        XCTAssertNotNil(viewModelForTest?.itemForProduct)
    }
    
    func testImageCount() {
        viewModelForTest?.loadData()
        sleep(3)
        XCTAssertTrue((viewModelForTest?.imageCount)! > 0)
    }
    
    func testImageDownload() {
        viewModelForTest?.loadData()
        sleep(3)
        let testIndexPath = IndexPath(item: 0, section: 0)
        viewModelForTest?.requestImageDownload(atIndexPath:testIndexPath )
        sleep(3)
        let image = viewModelForTest?.itemForProduct?.images?[testIndexPath]
        XCTAssertNotNil(image)
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
