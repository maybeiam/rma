//
//  PRODUCTTests.swift
//  RMA
//
//  Created by Shepherd on 2016. 10. 8..
//  Copyright © 2016년 Jin. All rights reserved.
//

import XCTest
@testable import RMA

class PRODUCTTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testInit() {
        let expectation = self.expectation(description: "Expectation for AllValues")
        let session = URLSession.shared
        let url = URL( string: "https://api.redmart.com/v1.5.6/products/51231" )
        let request = URLRequest(url: url!)
        let task = session.dataTask(with: request) { (d : Data?, r : URLResponse?, error : Error? ) in
            let result = try! JSONSerialization.jsonObject(with: d!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
            let dictionary = result.object(forKey: "product") as! NSDictionary
            let object = PRODUCT(withDictionary: dictionary)
            
            XCTAssertTrue(object.title == "Citylife 30L Storage Container With Wheels")
            XCTAssertTrue(object.img?.name == "/i/m/51231_1459416438682.jpg")
            XCTAssertTrue(object.desc == "One of Citylifes most popular product, Citylife 30L storage container has a sleek design for home and office use, stackable design allows you to stack multiple boxes without worrying that it will topple. Made of virgin pp material486 x 358 x 297mm.")
            XCTAssertTrue(object.pricing?.price == "16.9")
            expectation.fulfill()
        }
        task.resume()
        self.waitForExpectations(timeout: 3.0) { (e : Error?) in
            
        }
    }
    
    func testImages() {
        let object = PRODUCT()
        
        XCTAssertNil(object.images)
    }
    
    func testImagesWithAPICall() {
        let expectation = self.expectation(description: "Expectation for IMAGES ARRAY")
        let session = URLSession.shared
        let url = URL( string: "https://api.redmart.com/v1.5.6/products/51231" )
        let request = URLRequest(url: url!)
        let task = session.dataTask(with: request) { (d : Data?, r : URLResponse?, error : Error? ) in
            let result = try! JSONSerialization.jsonObject(with: d!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
            let dictionary = result.object(forKey: "product") as! NSDictionary
            let object = PRODUCT(withDictionary: dictionary)
            
            XCTAssertTrue((object.images?.count)! > 0 )
            XCTAssertNotNil(object.images?[0])
            expectation.fulfill()
        }
        task.resume()
        self.waitForExpectations(timeout: 3.0) { (e : Error?) in
            
        }
    }
    
    func testPRODUCTID() {
        let expectation = self.expectation(description: "Expectation for PRODUCTID")
        let session = URLSession.shared
        let url = URL( string: "https://api.redmart.com/v1.5.6/products/51231" )
        let request = URLRequest(url: url!)
        let task = session.dataTask(with: request) { (d : Data?, r : URLResponse?, error : Error? ) in
            let result = try! JSONSerialization.jsonObject(with: d!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
            let dictionary = result.object(forKey: "product") as! NSDictionary
            let object = PRODUCT(withDictionary: dictionary)
            XCTAssertTrue(object.id == "51231" )
            expectation.fulfill()
        }
        task.resume()
        self.waitForExpectations(timeout: 3.0) { (e : Error?) in
            
        }
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
