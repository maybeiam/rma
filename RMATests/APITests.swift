//
//  APITests.swift
//  RMA
//
//  Created by Shepherd on 2016. 10. 9..
//  Copyright © 2016년 Jin. All rights reserved.
//

import XCTest
@testable import RMA

class APITests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testAPI_CATALOG() {
        let expectation = self.expectation(description: "Expectation for item count 30")
        
        HttpRequest.requestGet(nil, targetUrl: API_CATALOG, success: { (jsonObject : NSDictionary?) in
                XCTAssertNotNil(jsonObject?.object(forKey: "products"))
                XCTAssertNotNil(jsonObject?.object(forKey: "page"))
                XCTAssertNotNil(jsonObject?.object(forKey: "page_size"))
                expectation.fulfill()
            }, failure: { (error : NSError?) in
                
            }, parametersObjectsAndKeys: "all-sales", "theme", "30", "pageSize", "0", "page" )
        
        self.waitForExpectations(timeout: 5.0) { (e : Error?) in
            
        }
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
