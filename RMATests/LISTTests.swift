//
//  LISTTests.swift
//  RMA
//
//  Created by Shepherd on 2016. 10. 8..
//  Copyright © 2016년 Jin. All rights reserved.
//

import XCTest
@testable import RMA

class LISTTests: XCTestCase {
    
    var list : LIST<NSObject>?
    var listForMore : LIST<NSObject>?
    override func setUp() {
        super.setUp()
        
        let session = URLSession.shared
        let url = URL( string: "https://api.redmart.com/v1.5.6/catalog/search?theme=all-sales&pageSize=30&page=0" )
        let request = URLRequest(url: url!)
        let task = session.dataTask(with: request) { (d : Data?, r : URLResponse?, error : Error? ) in
            let result = try! JSONSerialization.jsonObject(with: d!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
            self.listForMore = LIST(withProductDictionary: result)
            XCTAssertTrue(self.listForMore?.itemCount == 30)
            XCTAssertTrue(self.listForMore?.page == 0)
        }
        task.resume()
        sleep( 5 )
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testInit() {
        let expectation = self.expectation(description: "Expectation for item count 30")
        let session = URLSession.shared
        let url = URL( string: "https://api.redmart.com/v1.5.6/catalog/search?theme=all-sales&pageSize=30&page=0" )
        let request = URLRequest(url: url!)
        let task = session.dataTask(with: request) { (d : Data?, r : URLResponse?, error : Error? ) in
            let result = try! JSONSerialization.jsonObject(with: d!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
            self.list = LIST(withProductDictionary: result)
            XCTAssertTrue(self.list?.itemCount == 30)
            XCTAssertTrue(self.list?.page == 0)
            expectation.fulfill()
        }
        task.resume()
        self.waitForExpectations(timeout: 5.0) { (e : Error?) in
            
        }
    }
    
    func testAdd() {
        
        
        let session = URLSession.shared
        let url = URL( string: "https://api.redmart.com/v1.5.6/catalog/search?theme=all-sales&pageSize=30&page=1" )
        let request = URLRequest(url: url!)
        let task = session.dataTask(with: request) { (d : Data?, r : URLResponse?, error : Error? ) in
            let result = try! JSONSerialization.jsonObject(with: d!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
            let array = self.listForMore?.add(productDictionary: result)
            XCTAssertTrue((array?.count)! > 30)
            XCTAssertTrue((self.listForMore?.itemCount)! > 30)
            XCTAssertTrue(self.listForMore?.page == 1)
        }
        task.resume()
        sleep(5)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
