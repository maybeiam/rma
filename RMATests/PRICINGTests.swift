//
//  PRICINGTests.swift
//  RMA
//
//  Created by Shepherd on 2016. 10. 8..
//  Copyright © 2016년 Jin. All rights reserved.
//

import XCTest
@testable import RMA

class PRICINGTests: XCTestCase {
    var pricing : PRICING?
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInit() {
        let expectation = self.expectation(description: "Expectation for IMAGES ARRAY")
        let session = URLSession.shared
        let url = URL( string: "https://api.redmart.com/v1.5.6/products/51231" )
        let request = URLRequest(url: url!)
        let task = session.dataTask(with: request) { (d : Data?, r : URLResponse?, error : Error? ) in
            let result = try! JSONSerialization.jsonObject(with: d!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
            let dictionary = (result.object(forKey: "product") as! NSDictionary).object(forKey: "pricing") as! NSDictionary
            let object = PRICING(withDictionary: dictionary )
            
            XCTAssertTrue(object.on_sale == true )
            XCTAssertTrue(object.price == "16.9" )
            XCTAssertTrue(object.promo_price == "10.15" )
            XCTAssertTrue(object.savings_text == "40% OFF" )
            expectation.fulfill()
        }
        task.resume()
        self.waitForExpectations(timeout: 3.0) { (e : Error?) in
            
        }
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testVariables() {
//        XCTAssertTrue()
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
