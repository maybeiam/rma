//
//  ViewModelListTests.swift
//  RMA
//
//  Created by Shepherd on 2016. 10. 8..
//  Copyright © 2016년 Jin. All rights reserved.
//

import XCTest
@testable import RMA

class MockLIST : NSObject {
    var page = 0
    var pageSize = 30
    var items = Array<NSObject>()
    
    override init() {
//        self.init()
        for _ in 0 ..< 30 {
            items.append(NSObject())
        }
    }
}

class ViewModelListTests: XCTestCase {
    
    var list : ViewModelList<NSObject>?
    override func setUp() {
        super.setUp()
        list = ViewModelList(withPageSize: 30 )
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPage() {
        XCTAssertTrue(list?.page == 0)
    }
    
    func testViewModelListVariables() {
        XCTAssertTrue(list?.page == 0)
        XCTAssertTrue(list?.hasMore == false)
    }
    
    func testItemCount() {
        XCTAssertTrue(list?.itemCount == 0)
    }
    
    func testAppendItem() {
        list?.append( item : NSObject() )
        XCTAssertTrue(list?.itemCount == 1)
    }
    
    func testMore() {
        let origin = MockLIST()
        for item in origin.items {
            list?.append( item : item )
        }
        XCTAssertTrue(list?.itemCount == 30)
        XCTAssertTrue(list?.hasMore == true)
    }
    
    func testSMore() {
        let origin = MockLIST()
        list?.pageSize = origin.pageSize
        for item in origin.items {
            list?.append( item : item )
        }
        XCTAssertTrue(list?.itemCount == 30)
        XCTAssertTrue(list?.hasMore == true)
        for _ in 0 ..< 10 {
            list?.append(item: NSObject() )
        }
        XCTAssertTrue(list?.itemCount == 40)
        XCTAssertTrue(list?.hasMore == false)
        
        for _ in 0 ..< 19 {
            list?.append(item: NSObject() )
        }
        XCTAssertTrue(list?.itemCount == 59)
        XCTAssertTrue(list?.hasMore == false)
        
        for _ in 0 ..< 1 {
            list?.append(item: NSObject() )
        }
        XCTAssertTrue(list?.itemCount == 60)
        XCTAssertTrue(list?.hasMore == true)
    }
    
//    func testRemoveItem() {
//        list?.appendItem()
//        XCTAssertTrue(list?.itemCount == 1)
//        list?.removeItem()
//        XCTAssertTrue(list?.itemCount == 0)
//    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
