//
//  IMGTests.swift
//  RMA
//
//  Created by Shepherd on 2016. 10. 8..
//  Copyright © 2016년 Jin. All rights reserved.
//

import XCTest
@testable import RMA

class IMGTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testInit() {
        let expectation = self.expectation(description: "Expectation for IMG")
        let session = URLSession.shared
        let url = URL( string: "https://api.redmart.com/v1.5.6/catalog/search?theme=all-sales&pageSize=30&page=0" )
        let request = URLRequest(url: url!)
        let task = session.dataTask(with: request) { (d : Data?, r : URLResponse?, error : Error? ) in
            let result = try! JSONSerialization.jsonObject(with: d!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary
            let array = result.object(forKey: "products") as! NSArray
            let dictionary = (array[0] as! NSDictionary).object(forKey: "img") as! NSDictionary
            XCTAssertNotNil(dictionary.object(forKey: "h"))
            let object = IMG(withDictionary: dictionary)
            
            XCTAssertTrue(object.h == 0.0)
            XCTAssertTrue(object.name == "/i/m/8888086775812_0026_1471499723036.jpg")
            XCTAssertTrue(object.w == 0.0)
            XCTAssertTrue(object.position == 0)
            expectation.fulfill()
        }
        task.resume()
        self.waitForExpectations(timeout: 5.0) { (e : Error?) in
            
        }
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
